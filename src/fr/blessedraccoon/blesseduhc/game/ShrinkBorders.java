package fr.blessedraccoon.blesseduhc.game;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import fr.blessedraccoon.blessedtimer.timer.StandardTimer;
import fr.blessedraccoon.blessedtimer.timer.TimeListener;
import net.md_5.bungee.api.ChatColor;

public class ShrinkBorders implements TimeListener {
	
	private int triggerTime;
	
	public ShrinkBorders(int time) {
		this.triggerTime = time;
	}

	@Override
	public int getTriggerTime() {
		return triggerTime;
	}

	@Override
	public void trigger(StandardTimer timer, long time) {
		UHCManager.getInstance().getWorldborder().shrink();
        // ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
        Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
        final String message = "Les bordures commencent à se rétrécir !";
        for (Player player : players) {
            player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_AMBIENT, 1.0f, 1.0f);
            player.sendTitle(ChatColor.RED + message, "Dirigez-vous vers le centre !", 10, 70, 20);
        }
        
        // Bukkit.getServer().dispatchCommand(console, "title @a title {\"text\":\"" + message + "\", \"color\":\"red\"}");
        Bukkit.broadcastMessage(message);
	}

	public void setTrigger(int trigger) {
		this.triggerTime = trigger;
	}

}
