package fr.blessedraccoon.blesseduhc.game;

/**
 * Base of UHC<p>
 * Every UHC Manager or UHC Scenario must implement this interface
 *
 */
public interface UHC {
	
	/**
	 * Prepares the UHC (Mandatory to start
	 * @return true if everything has gone well
	 */
	public void prepare() throws UHCException;
	
	/**
	 * Starts the UHC. It completes all tasks that are needed for the UHC.
	 * @return true if everything has gone well
	 */
	public void start() throws UHCException;
}
