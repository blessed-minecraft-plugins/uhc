package fr.blessedraccoon.blesseduhc.game;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import fr.blessedraccoon.blessedtimer.timer.StandardTimer;
import fr.blessedraccoon.blessedtimer.timer.TimeListener;
import fr.blessedraccoon.blesseduhc.BlessedUHC;
import net.md_5.bungee.api.ChatColor;

public class AllowPvP implements TimeListener {
	
	private int triggerTime;
	
	public AllowPvP(int triggerTime) {
		this.setTriggerTime(triggerTime);
	}

	@Override
	public int getTriggerTime() {
		return this.triggerTime;
	}

	@Override
	public void trigger(StandardTimer timer, long time) {
		BlessedUHC.getOverworld().setPVP(true);
        BlessedUHC.getNether().setPVP(true);
        BlessedUHC.getEnd().setPVP(true);
        Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
        final String message = "Le PvP est désormais activé !";
        for (Player player : players) {
            player.playSound(player.getLocation(), Sound.ENTITY_WITHER_DEATH, 1.0f, 1.0f);
            player.sendTitle(ChatColor.RED + message, "Bonne chance !", 10, 70, 20);
        }
        // Bukkit.getServer().dispatchCommand(console, "title @a title {\"text\":\"" + message + "\", \"color\":\"red\"}");
        Bukkit.broadcastMessage(message);

	}

	public void setTriggerTime(int triggerTime) {
		this.triggerTime = triggerTime;
	}

}
