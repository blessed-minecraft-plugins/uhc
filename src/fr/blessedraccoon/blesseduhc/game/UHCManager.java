package fr.blessedraccoon.blesseduhc.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.GameRule;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Team;

import fr.blessedraccoon.blessedtimer.timer.StandardTimer;
import fr.blessedraccoon.blessedtimer.timer.TimerInvalidNameException;
import fr.blessedraccoon.blessedtimer.timer.TimerManager;
import fr.blessedraccoon.blessedtimer.timer.TimerMethod;
import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.commands.Configure;
import fr.blessedraccoon.blesseduhc.commands.Option;
import fr.blessedraccoon.blesseduhc.commands.Prepare;
import fr.blessedraccoon.blesseduhc.commands.Recap;
import fr.blessedraccoon.blesseduhc.commands.Start;
import fr.blessedraccoon.blesseduhc.commands.Stop;
import fr.blessedraccoon.blesseduhc.commands.UHCHelp;
import fr.blessedraccoon.blesseduhc.commands.UnStop;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;
import fr.blessedraccoon.blesseduhc.game.options.UHCOptions;
import fr.blessedraccoon.commandmanager.CommandManager;

public final class UHCManager extends CommandManager implements UHC {
	
	public static final String UHC_TIMER_NAME = "UHC_TIME";
	private StandardTimer timer;

    private static UHCManager instance;
    private UHC fullGame;
    private WorldBorderControl worldborder;
    private AllowPvP pvp;
    private SpreadPlayers spreadPlayers;
    private boolean started;
    
    private SpacePoint center;
    
    private boolean prepared;

    private UHCManager() {
    	try {
			TimerManager.getInstance().createTimer(UHC_TIMER_NAME);
		} catch (TimerInvalidNameException e) {
			TimerManager.getInstance().getTimer(UHC_TIMER_NAME).setCountMethod(new TimerMethod());
			TimerManager.getInstance().getTimer(UHC_TIMER_NAME).reset();
		}
    	this.prepared = false;
        this.timer = TimerManager.getInstance().getTimer(UHC_TIMER_NAME);
    	this.started = false;
        this.spreadPlayers = new SpreadPlayers();
        this.worldborder = new WorldBorderControl();
        
        this.pvp = new AllowPvP(BlessedUHC.CONFIG.getInt("gameplay.pvpTime"));
        timer.addTrigger(this.pvp);
        timer.addTrigger(this.worldborder.getShrinkBorders());
        
        this.setCenter(new SpacePoint(
        		BlessedUHC.CONFIG.getInt("gameplay.spawn.x"),
        		BlessedUHC.CONFIG.getInt("gameplay.spawn.y"),
        		BlessedUHC.CONFIG.getInt("gameplay.spawn.z")
        ));
        
        // SUBCOMMANDS
        this.addSubcommand(new Configure());
        this.addSubcommand(new Start());
        this.addSubcommand(new Stop());
        this.addSubcommand(new UnStop());
        this.addSubcommand(new Prepare());
        this.addSubcommand(new Option());

        this.addSubcommand(new Recap(this));
        this.addSubcommand(new UHCHelp(this));
        
    }
    
    public void drawOptions() {
    	for(UHCOptions option: UHCOptions.values()) {
    		option.getOption().setUHC(null);
    	}
    	this.fullGame = this;
    	
    	List<Integer> forbidden = new ArrayList<>();
    	for(UHCOptions option: UHCOptions.reverseValues()) {
    		Integer in = Integer.valueOf(option.getOption().getID());
    		if (! forbidden.contains(in)) {
    			if (OptionRarity.test(option.getOption().getRarity())) {
        			this.addOption(option.getOption().getID());
        			forbidden.addAll(option.getForbidden());
        		}
    		}
    	}
    }
    
    public void recapAllOptions(Player p) {
    	p.sendMessage("--------------------------------------------");
    	for(UHCOption option: UHCOptions.getSortedOptions(UHCOptions.RARITY_SORT)) {
    		this.recapOption(p, option);
    	}
    	p.sendMessage("--------------------------------------------");
    }
    
    public void recapCurrentOptions(Player p) {
    	p.sendMessage("--------------------------------------------");
    	if (this.fullGame instanceof UHCOption) {
    		List<UHCOption> recap = ((UHCOption) this.fullGame).recap();
    		UHCOptions.getSortedOptions(recap, UHCOptions.RARITY_SORT);
    		for(UHCOption rec: recap) {
    			p.sendMessage(rec.toTextComponent());
    		}
    	}
    	else {
    		p.sendMessage("Aucune option activée pour l'instant");
    	}
    	p.sendMessage("--------------------------------------------");
    	
    }
    
    public void recapOption(Player p, UHCOption option) {
		p.sendMessage(option.toTextComponent());
    }
    
    @Override
    public void prepare() {
    	GameTeam.createAll();
    	prepared = true;
    }

    @Override
    public void start() {

        // Sets the border at its initial location
        this.worldborder.start();

        // Sets the difficulty to Hard
        BlessedUHC.getOverworld().setDifficulty(Difficulty.HARD);
        BlessedUHC.getNether().setDifficulty(Difficulty.HARD);
        BlessedUHC.getEnd().setDifficulty(Difficulty.HARD);

        // Give players Resistance 80 and Regeneration 3 for 30 seconds
        // Pass players in gamemode Survival
        List<Player> players = GameTeam.getPlayingPlayers();
        for (Player player : players) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 600, 80));
            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 600, 3));
            player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 600, 3));
            player.setGameMode(GameMode.SURVIVAL);
        }

        // spread players around the map
        this.spreadPlayers.spread();

        // Set natural Regeneration to false
        BlessedUHC.getOverworld().setGameRule(GameRule.NATURAL_REGENERATION, false);
        BlessedUHC.getNether().setGameRule(GameRule.NATURAL_REGENERATION, false);
        BlessedUHC.getEnd().setGameRule(GameRule.NATURAL_REGENERATION, false);

        // Time set day
        System.out.println("Time set day");
        BlessedUHC.getOverworld().setTime(1000);

        // Disable PVP
        System.out.println("Disable PvP");
        BlessedUHC.getOverworld().setPVP(false);
        BlessedUHC.getNether().setPVP(false);
        BlessedUHC.getEnd().setPVP(false);

        // Resets the timer
        System.out.println("Resets timer");
        timer.reset();

        // Sets the timer visible
        System.out.println("Sets the timer visible");
        TimerManager.getInstance().setVisible(timer);

        // Runs the timer
        System.out.println("Runs the timer");
        timer.setRunning(true);

        // Disallow friendly fire between members of team
        System.out.println("Disallow friendly fire");
        Set<Team> teams = Bukkit.getScoreboardManager().getMainScoreboard().getTeams();
        for (Team team : teams) {
            team.setAllowFriendlyFire(false);
        }

        // Clears all players inventories
        // TODO Don't know how to handle this, because it can be embarassing if we need to restart, and the stuff is lost...
        // Bukkit.getServer().dispatchCommand(console, "clear @a");

        // Starts the check
        this.started = true;
    }
    
    public void addOption(int ID) {
    	for(UHCOptions option: UHCOptions.values()) {
    		if (ID == option.getOption().getID() && ! hasOption(fullGame, ID)) {
    			option.getOption().setUHC(this.fullGame);
        		this.fullGame = option.getOption();
        	}
    	}
    	
    }
    
    public void removeOption(int ID) {
    	if (this.fullGame instanceof UHCOption) {
    		UHCOption game = ((UHCOption) this.fullGame);
    		if (game.getID() == ID && game.getNested() == this) {
    			game.setUHC(null);
    			this.fullGame = this;
    		} else {
    			((UHCOption) this.fullGame).removeOption(ID);
    		}
    		
    	}
    }
    
	private boolean hasOption(UHC current, int ID) {
    	if (current != null && current instanceof UHCOption) {
    		UHCOption option = (UHCOption) current;
    		if (option.getID() == ID) {
    			return true;
    		}
    		else {
    			return hasOption(option.getNested(), ID);
    		}
    	}
    	return false;
    }

    @Override
    public int getDeepLevel() {
        return 0;
    }

    @Override
    public String getDescription() {
        return "main command, to manage UHC games.";
    }

    @Override
    public String getName() {
        return "uhc";
    }

    @Override
    public String getSyntax() {
        return "/uhc";
    }

    @Override
    public boolean perform(Player arg0, String[] arg1) {
        return false;
    }

    public boolean isStarted() {
        return this.started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public SpreadPlayers getSpreadPlayers() {
        return this.spreadPlayers;
    }

    public WorldBorderControl getWorldborder() {
        return this.worldborder;
    }

	@Override
	public String getPermission() {
		return "uhc";
	}
	
	public UHC getFullGame() {
		return this.fullGame;
	}
	
	public static synchronized final UHCManager getInstance() {
		if (instance == null) {
			instance = new UHCManager();
			instance.fullGame = instance;
			
			// Enabling all enabled options in config
			// (Can't move this into the constructor: infinite loop)
	        for(UHCOptions option: UHCOptions.values()) {
	        	option.getOption().manageFromConfig();
	        }
		}
		return instance;
	}

	public StandardTimer getTimer() {
		return timer;
	}

	public boolean isPrepared() {
		return prepared;
	}

	public SpacePoint getCenter() {
		return center;
	}

	public void setCenter(SpacePoint center) {
		this.center = center;
	}
	
	public AllowPvP getAllowPvp() {
		return this.pvp;
	}

}
