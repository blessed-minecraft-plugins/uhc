package fr.blessedraccoon.blesseduhc.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public enum GameTeam {
	
	DARK_RED("dark_red", ChatColor.DARK_RED, true),
	RED("red", ChatColor.RED, true),
	GOLD("gold", ChatColor.GOLD, true),
	YELLOW("yellow", ChatColor.YELLOW, true),
	DARK_GREEN("dark_green", ChatColor.DARK_GREEN, true),
	GREEN("green", ChatColor.GREEN, true),
	AQUA("aqua", ChatColor.AQUA, true),
	DARK_AQUA("dark_aqua", ChatColor.DARK_AQUA, true),
	DARK_BLUE("dark_blue", ChatColor.DARK_BLUE, true),
	BLUE("blue", ChatColor.BLUE, true),
	LIGHT_PURPLE("light_purple", ChatColor.LIGHT_PURPLE, true),
	DARK_PURPLE("dark_purple", ChatColor.DARK_PURPLE, true),
	WHITE("white", ChatColor.WHITE, true),
	GRAY("gray", ChatColor.GRAY, true),
	DARK_GRAY("dark_gray", ChatColor.DARK_GRAY, true),
	BLACK("black", ChatColor.BLACK, true),
	MODERATOR("moderator", ChatColor.BOLD, false),
	SPECTATOR("spectator", ChatColor.ITALIC, false);
	
	
	GameTeam(String name, ChatColor color, boolean playing) {
		this.name = name;
		this.color = color;
		this.playing = playing;
	}

	private String name;
	private ChatColor color;
	private boolean playing;
	
	public static void createAll() {
		Scoreboard sb = Bukkit.getScoreboardManager().getMainScoreboard();
		for(GameTeam gameTeam : GameTeam.values()) {
			if (sb.getTeam(gameTeam.name) == null) {
				sb.registerNewTeam(gameTeam.name);
				sb.getTeam(gameTeam.name).setColor(gameTeam.color);
			}
		}
	}
	
	public static Team toTeam(GameTeam gt) {
		return  Bukkit.getScoreboardManager().getMainScoreboard().getTeam(gt.name);
	}
	
	public static boolean existsTeam(GameTeam gt) {
		return Bukkit.getScoreboardManager().getMainScoreboard().getTeam(gt.getName()) != null;
	}
	
	public static List<GameTeam> getNonPlayingTeams() {
		return Arrays.stream(GameTeam.values()).filter(t -> ! t.isPlaying()).collect(Collectors.toList());
	}
	
	public static List<GameTeam> getPlayingTeams() {
		return Arrays.stream(GameTeam.values()).filter(t -> t.isPlaying()).collect(Collectors.toList());
	}
	
	public static List<Player> getOnlinePlayersOfTeam(GameTeam gt) {
		Team team = GameTeam.toTeam(gt);
		return GameTeam.getOnlinePlayersFromEntries(team.getEntries());
	}
	
	public static List<Player> getOnlinePlayersFromEntries(Collection<String> entries) {
		return entries.stream().map(GameTeam::getOnlinePlayerFromEntry).filter(Objects::nonNull).collect(Collectors.toList());
	}
	
	public static Player getOnlinePlayerFromEntry(String name) {
		return Bukkit.getPlayer(name);
	}
	
	public static List<Player> getPlayingPlayers() {
		List<Player> players = new ArrayList<Player>();
		for(GameTeam team : GameTeam.getPlayingTeams()) {
			players.addAll(getOnlinePlayersOfTeam(team));
		}
		return players;
	}
	
	public static List<Team> getTeamsWithMinOnlinePlayers(int minPlayers) {
		return GameTeam.getPlayingTeams().stream()
			.map(GameTeam::toTeam)
			.filter(t -> t.getEntries().size() >= minPlayers)
			.collect(Collectors.toList());
	}
	
	public static List<Player> getNonPlayingPlayers() {
		List<Player> players = new ArrayList<Player>();
		for(GameTeam team : GameTeam.getNonPlayingTeams()) {
			players.addAll(getOnlinePlayersOfTeam(team));
		}
		return players;
	}
	
	public static List<Player> getOnlinePlayersOfTeam(Team team) {
		return GameTeam.getOnlinePlayersFromEntries(team.getEntries());
	}
	
	public static List<Player> getAdminPlayers() {
		List<Player> players = new ArrayList<Player>();
		if (GameTeam.existsTeam(GameTeam.MODERATOR)) {
			players.addAll(GameTeam.toTeam(GameTeam.MODERATOR).getEntries().stream().map(e -> Bukkit.getPlayer(e)).filter(Objects::nonNull).collect(Collectors.toList()));
		}
		return players;
	}
	
	public String getName() {
		return name;
	}

	public ChatColor getColor() {
		return color;
	}

	public boolean isPlaying() {
		return playing;
	}

}
