package fr.blessedraccoon.blesseduhc.game;

import org.bukkit.WorldBorder;
import fr.blessedraccoon.blesseduhc.BlessedUHC;

public class WorldBorderControl {

    private WorldBorder worldBorder = BlessedUHC.getOverworld().getWorldBorder();

    private ShrinkBorders shrinkBorders;
    private int beginSize;
    private int endSize;
    private int shrinkDuration;
    
    public WorldBorderControl() {
        this(
        	BlessedUHC.CONFIG.getInt("gameplay.borders.beginSize"),
        	BlessedUHC.CONFIG.getInt("gameplay.borders.endSize"),
        	BlessedUHC.CONFIG.getInt("gameplay.borders.beginShrinkTime"),
        	BlessedUHC.CONFIG.getInt("gameplay.borders.shrinkDuration")
        );
    }

    public WorldBorderControl(int beginSize, int endSize, int beginShrinkTime, int shrinkDuration) {
        this.beginSize = beginSize;
        this.endSize = endSize;
        this.shrinkBorders = new ShrinkBorders(beginShrinkTime);
        this.shrinkDuration = shrinkDuration;
    }

    public int getShrinkDuration() {
        return this.shrinkDuration;
    }

    public void setShrinkDuration(int shrinkDuration) {
        this.shrinkDuration = shrinkDuration;
    }

    public int getEndSize() {
        return endSize;
    }

    public void setEndSize(int endSize) {
        this.endSize = endSize;
    }

    public int getBeginSize() {
        return beginSize;
    }

    public void setBeginSize(int beginSize) {
        this.beginSize = beginSize;
    }

    public void start() {
    	SpacePoint center = UHCManager.getInstance().getCenter();
        this.worldBorder.setCenter(center.getX(), center.getZ());
        this.worldBorder.setSize(this.beginSize);
    }

    public void shrink() {
        this.worldBorder.setSize(this.endSize, this.shrinkDuration);
    }

	public ShrinkBorders getShrinkBorders() {
		return shrinkBorders;
	}

}
