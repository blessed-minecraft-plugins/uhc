package fr.blessedraccoon.blesseduhc.game;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;

import fr.blessedraccoon.blesseduhc.BlessedUHC;

public class SpreadPlayers {

    private int minDistance; // The minimum distance between players / teams.
    private int maxRange; // The maximum range (applies to x and z coordinates.
    private boolean respectTeams; // Whether players in teams should be teleported to the same location (if applicable).

    public SpreadPlayers() {
        this(
        	BlessedUHC.CONFIG.getInt("gameplay.spread.minDistance"),
        	BlessedUHC.CONFIG.getInt("gameplay.spread.maxRange"),
        	BlessedUHC.CONFIG.getBoolean("gameplay.spread.respectTeams")
        );
    }
    
    public SpreadPlayers(int minDistance, int maxRange, boolean respectTeams) {
        this.minDistance = 200;
        this.maxRange = 500;
        this.respectTeams = true;
    }

    public boolean isRespectTeams() {
        return respectTeams;
    }

    public void setRespectTeams(boolean respectTeams) {
        this.respectTeams = respectTeams;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public void setMaxRange(int maxRange) {
        this.maxRange = maxRange;
    }

    public int getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(int minDistance) {
        this.minDistance = minDistance;
    }

    public void spread() {
        ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
        List<String> teamNames = GameTeam.getNonPlayingTeams().stream().map(t -> t.getName()).collect(Collectors.toList());
        String teams = "@a[team=!" + String.join(",team=!", teamNames) + "]";
        final int x = UHCManager.getInstance().getCenter().getX();
        final int z = UHCManager.getInstance().getCenter().getZ();
        Bukkit.getServer().dispatchCommand(console, String.format("spreadplayers %d %d %d %d %b %s", x, z, this.minDistance, this.maxRange, this.respectTeams, teams));
    }
    
}
