package fr.blessedraccoon.blesseduhc.game;

@SuppressWarnings("serial")
/**
 * Modelizes and error, that can be thrown while using this plugin.
 */
public class UHCException extends Exception {

	public UHCException(String message) {
		super(message);
	}
	
}
