package fr.blessedraccoon.blesseduhc.game;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import fr.blessedraccoon.blessedtimer.timer.StandardTimer;

public class DeathControl implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onDeath(PlayerDeathEvent event) {
        StandardTimer timer = UHCManager.getInstance().getTimer();
        Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
        for (Player player : players) {
            player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1.0f, 1.0f);
        }
        event.setDeathMessage(event.getDeathMessage() + ", at " + ChatColor.GOLD + timer.toString());
        Player player = event.getEntity();
        player.teleport(new Location(player.getWorld(), 0, 100, 0));
        player.setGameMode(GameMode.SPECTATOR);
    }
    
}
