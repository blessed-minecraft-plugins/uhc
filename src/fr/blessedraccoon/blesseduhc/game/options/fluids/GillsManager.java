package fr.blessedraccoon.blesseduhc.game.options.fluids;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class GillsManager extends UHCOption {

	public GillsManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.DROWNING_DAMAGE, false);
		BlessedUHC.getNether().setGameRule(GameRule.DROWNING_DAMAGE, false);
		BlessedUHC.getEnd().setGameRule(GameRule.DROWNING_DAMAGE, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.LEGENDARY;
	}

	@Override
	public String getDescription() {
		return "You cannot drown: you have unlimited breath while in liquids";
	}

	@Override
	public String getName() {
		return "Gills";
	}

}
