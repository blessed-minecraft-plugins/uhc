package fr.blessedraccoon.blesseduhc.game.options;

import org.bukkit.ChatColor;

public enum OptionRarity {
	
	UNFINDABLE(0, ChatColor.ITALIC),
	COMMON(4.0 / 5.0, ChatColor.WHITE),
	UNCOMMON(9.0 / 20.0, ChatColor.GREEN),
	RARE(1.0 / 5.0, ChatColor.DARK_BLUE),
	EPIC(1.0 / 12.5, ChatColor.DARK_PURPLE),
	LEGENDARY(1.0 / 40.0, ChatColor.GOLD),
	MYTHIC(1.0 / 150.0, ChatColor.DARK_RED);
	
	private OptionRarity(double rate, ChatColor color) {
		if (rate >= 1 || rate < 0) {
			rate = 0.5;
		}
		this.rate = rate;
		this.color = color;
	}
	
	public static boolean test(OptionRarity rarity) {
		return Math.random() < rarity.getRate();
	}
	
	public static boolean test(double rarity) {
		return Math.random() < rarity;
	}
	
	public double getRate() {
		return rate;
	}

	public ChatColor getColor() {
		return color;
	}

	private double rate;
	private ChatColor color;

}
