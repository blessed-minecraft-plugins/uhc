package fr.blessedraccoon.blesseduhc.game.options.eternalday;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class EternalDayManager extends UHCOption {

	public EternalDayManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		BlessedUHC.getOverworld().setTime(6000);
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
		BlessedUHC.getOverworld().setTime(6000);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.UNCOMMON;
	}

	@Override
	public String getName() {
		return "EternalDay";
	}
	
	@Override
	public String getDescription() {
		return "Settles Eternal Day during the event.";
	}

}
