package fr.blessedraccoon.blesseduhc.game.options;

final class OptionIDGenerator {
	
	private static int currID = 0;
	
	public static final int generate() {
		return ++currID;
	}

}
