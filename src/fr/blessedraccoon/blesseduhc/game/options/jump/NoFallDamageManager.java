package fr.blessedraccoon.blesseduhc.game.options.jump;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class NoFallDamageManager extends UHCOption {

	public NoFallDamageManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {

	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.FALL_DAMAGE, false);
		BlessedUHC.getNether().setGameRule(GameRule.FALL_DAMAGE, false);
		BlessedUHC.getEnd().setGameRule(GameRule.FALL_DAMAGE, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.RARE;
	}

	@Override
	public String getDescription() {
		return "Falling from a hidh place deals no damage";
	}

	@Override
	public String getName() {
		return "NoFallDamage";
	}

}
