package fr.blessedraccoon.blesseduhc.game.options.weather;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class NoRainManager extends UHCOption {

	public NoRainManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setStorm(false);
		BlessedUHC.getOverworld().setThundering(false);
		BlessedUHC.getOverworld().setGameRule(GameRule.DO_WEATHER_CYCLE, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.UNCOMMON;
	}

	@Override
	public String getDescription() {
		return "no rain nor thunder can appear";
	}

	@Override
	public String getName() {
		return "NoRain";
	}

}
