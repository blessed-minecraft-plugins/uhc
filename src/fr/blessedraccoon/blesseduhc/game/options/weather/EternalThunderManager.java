package fr.blessedraccoon.blesseduhc.game.options.weather;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class EternalThunderManager extends UHCOption {

	public EternalThunderManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setStorm(true);
		BlessedUHC.getOverworld().setThundering(true);
		BlessedUHC.getOverworld().setGameRule(GameRule.DO_WEATHER_CYCLE, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.EPIC;
	}

	@Override
	public String getDescription() {
		return "The storm and thunder rage through";
	}

	@Override
	public String getName() {
		return "EternalThunder";
	}

}
