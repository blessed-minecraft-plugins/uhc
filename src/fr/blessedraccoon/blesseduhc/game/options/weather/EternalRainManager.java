package fr.blessedraccoon.blesseduhc.game.options.weather;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class EternalRainManager extends UHCOption {

	public EternalRainManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setStorm(true);
		BlessedUHC.getOverworld().setThundering(false);
		BlessedUHC.getOverworld().setGameRule(GameRule.DO_WEATHER_CYCLE, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.RARE;
	}

	@Override
	public String getDescription() {
		return "It rains during the whole event";
	}

	@Override
	public String getName() {
		return "EternalRain";
	}

}
