package fr.blessedraccoon.blesseduhc.game.options.arrow3x;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.AbstractArrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.util.Vector;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class Arrow3XManager extends UHCOption implements Listener {

	public Arrow3XManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {

	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC plugin = BlessedUHC.getUHCPlugin();
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.EPIC;
	}

	@Override
	public String getDescription() {
		return "When an arrow is shot, three of them are shot instead. Whatever, a single one is consumed";
	}

	@Override
	public String getName() {
		return "3xArrows";
	}
	
	@EventHandler
	public void onArrowShot(EntityShootBowEvent e) {
		
		Player player = Bukkit.getPlayer(e.getEntity().getName());
		if (player != null) {
			Location loc = player.getLocation();
	        Vector direction = loc.getDirection();
	        if (e.getProjectile() instanceof AbstractArrow) {
	        	player.launchProjectile(((AbstractArrow) e.getProjectile()).getClass(), direction.add(this.getRandomVector()));
	        	player.launchProjectile(((AbstractArrow) e.getProjectile()).getClass(), direction.add(this.getRandomVector()));
	        }
		}
        
	}
	
	private Vector getRandomVector() {
		Random rand = new Random();
		return new Vector(
			rand.nextGaussian() / 5,
			rand.nextGaussian() / 5,	
			rand.nextGaussian() / 5
		);
	}

}
