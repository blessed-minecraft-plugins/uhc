package fr.blessedraccoon.blesseduhc.game.options.drops;

import java.util.Collection;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.drop.blocks.BlockBreakUtils;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class AppleDropPlusManager extends UHCOption implements Listener {
	
	private static final double DROP_RATE = .5;

	public AppleDropPlusManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC plugin = BlessedUHC.getUHCPlugin();
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.COMMON;
	}

	@Override
	public String getDescription() {
		return "Raises the apple drop rate";
	}

	@Override
	public String getName() {
		return "AppleDrop+";
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		if (e.getPlayer().getActiveItem().getType() != Material.SHEARS) {
			if (Math.random() < DROP_RATE) {
				Collection<Material> materials =  BlockBreakUtils.LEAVES.getValues();
				BlockBreakUtils.setDrop(e, materials, new ItemStack(Material.APPLE, 1));
			}
		}
		
	}

}
