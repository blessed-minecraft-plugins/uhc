package fr.blessedraccoon.blesseduhc.game.options.drops;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.drop.blocks.BlockBreakUtils;
import fr.blessedraccoon.blesseduhc.game.drop.blocks.DropMultiplicator;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class AppleFamineManager extends UHCOption implements Listener {

	public AppleFamineManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC plugin = BlessedUHC.getUHCPlugin();
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.EPIC;
	}

	@Override
	public String getDescription() {
		return "Apples no longer drop from leaves.";
	}

	@Override
	public String getName() {
		return "AppleFamine";
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		if (e.getPlayer().getActiveItem().getType() != Material.SHEARS) {
			BlockBreakUtils.modifyDrop(e, BlockBreakUtils.LEAVES.getValues(), new DropMultiplicator(0));
		}
	}

}
