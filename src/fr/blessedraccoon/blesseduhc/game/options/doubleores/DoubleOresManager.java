package fr.blessedraccoon.blesseduhc.game.options.doubleores;

import java.util.Collection;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.drop.blocks.BlockBreakUtils;
import fr.blessedraccoon.blesseduhc.game.drop.blocks.DropMultiplicator;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class DoubleOresManager extends UHCOption implements Listener {

	public DoubleOresManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {

	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC plugin = BlessedUHC.getUHCPlugin();
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.RARE;
	}

	@Override
	public String getDescription() {
		return "2x drops for all ores";
	}

	@Override
	public String getName() {
		return "DoubleOres";
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Collection<Material> materials =  BlockBreakUtils.ORES
				.not(Material.GOLD_ORE)
				.not(Material.IRON_ORE)
				.getValues();
		BlockBreakUtils.modifyDrop(e, materials, new DropMultiplicator(2));
	}

}
