package fr.blessedraccoon.blesseduhc.game.options.nophantom;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class NoPhantomManager extends UHCOption {

	public NoPhantomManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {

	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.DO_INSOMNIA, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.COMMON;
	}

	@Override
	public String getDescription() {
		return "No phantoms will appear";
	}

	@Override
	public String getName() {
		return "NoPhantom";
	}

}
