package fr.blessedraccoon.blesseduhc.game.options;

import fr.blessedraccoon.blesseduhc.game.options.moles.MoleManager;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import fr.blessedraccoon.blesseduhc.game.options.eternalday.EternalDayManager;
import fr.blessedraccoon.blesseduhc.game.options.eternalnight.EternalNightManager;
import fr.blessedraccoon.blesseduhc.game.options.halfores.HalfOresManager;
import fr.blessedraccoon.blesseduhc.game.options.doubleores.DoubleOresManager;
import fr.blessedraccoon.blesseduhc.game.options.tripleores.TripleOresManager;
import fr.blessedraccoon.blesseduhc.game.options.arrow3x.Arrow3XManager;
import fr.blessedraccoon.blesseduhc.game.options.weather.*;
import fr.blessedraccoon.blesseduhc.game.options.nophantom.NoPhantomManager;
import fr.blessedraccoon.blesseduhc.game.options.fire.*;
import fr.blessedraccoon.blesseduhc.game.options.mobs.*;
import fr.blessedraccoon.blesseduhc.game.options.jump.NoFallDamageManager;
import fr.blessedraccoon.blesseduhc.game.options.fluids.*;
import fr.blessedraccoon.blesseduhc.game.options.achievements.*;
import fr.blessedraccoon.blesseduhc.game.options.drops.*;

public enum UHCOptions {

	Moles(new MoleManager(null)),
	EternalDay(new EternalDayManager(null)),
	EternalNight(new EternalNightManager(null), EternalDay),
	DoubleOres(new DoubleOresManager(null)),
	TripleOres(new TripleOresManager(null)),
	HalfOres(new HalfOresManager(null), DoubleOres, TripleOres),
	TripleShot(new Arrow3XManager(null)),
	NoRain(new NoRainManager(null)),
	EternalRain(new EternalRainManager(null), NoRain),
	EternalThunder(new EternalThunderManager(null), NoRain, EternalRain),
	NoPhantom(new NoPhantomManager(null)),
	NoFireDamage(new NoFireDamageManager(null)),
	NoFireSpread(new NoFireSpreadManager(null)),
	NoMobGriefing(new NoMobGriefingManager(null)),
	NoMobLoot(new NoMobLootManager(null)),
	MobFree(new MobFreeManager(null), NoPhantom, NoMobGriefing, NoMobLoot),
	NoFallDamage(new NoFallDamageManager(null)),
	Gills(new GillsManager(null)),
	AchievementParanoia(new AchievementParanoiaManager(null)),
	AppleDropPlus(new AppleDropPlusManager(null)),
	AppleFamine(new AppleFamineManager(null), AppleDropPlus);
	
	private UHCOption option;
	private List<Integer> forbidden;
	
	private UHCOptions(UHCOption option, UHCOptions...forbidden) {
		this.option = option;
		this.forbidden = Arrays.stream(forbidden).map(f -> f.getOption().getID()).collect(Collectors.toList());
	}

	public UHCOption getOption() {
		return option;
	}
	
	public static final Comparator<UHCOption> RARITY_SORT = (opt1, opt2) -> {
		double difference = opt1.getRarity().getRate() - opt2.getRarity().getRate();
		if (difference > 0) return 1;
		else if (difference < 0) return -1;
		else return 0;
	};
	
	public static final Comparator<UHCOption> ALPHABETICAL_SORT = (opt1, opt2) -> {
		return opt1.getName().compareTo(opt2.getName());
	};
		
	
	public static List<UHCOption> getSortedOptions(Comparator<UHCOption> sorter) {
		List<UHCOption> options = Arrays.asList(UHCOptions.values()).stream().map(opt -> opt.getOption()).collect(Collectors.toList());
		Collections.sort(options, sorter);
		return options;
	}
	
	public static List<UHCOption> getSortedOptions(List<UHCOption> options, Comparator<UHCOption> sorter) {
		Collections.sort(options, sorter);
		return options;
	}

	public List<Integer> getForbidden() {
		return forbidden;
	}
	
	public static UHCOptions[] reverseValues() {
		UHCOptions[] options = values();
		for (int i = 0; i < values().length / 2; i++) {
			UHCOptions temp = options[i];
	        options[i] = options[options.length - 1 - i];
	        options[options.length - 1 - i] = temp;
	    }
		return options;
	}
}
