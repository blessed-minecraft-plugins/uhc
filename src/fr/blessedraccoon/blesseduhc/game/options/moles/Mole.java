package fr.blessedraccoon.blesseduhc.game.options.moles;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import net.md_5.bungee.api.ChatColor;

public class Mole {
	
	private Player player;
	private Team team;
	private boolean revealed;
	
	public Mole(Player player, Team team) {
		this.player = player;
		this.team = team;
		this.setRevealed(false);
	}
	
	public void sendMessage(String message) {
		this.getPlayer().sendMessage(ChatColor.GOLD + message);
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Team getTeam() {
		return team;
	}
	
	@Override
	public String toString() {
		return "Taupe : " + team.getColor() + player.getName();
	}

	public boolean isRevealed() {
		return revealed;
	}

	public void setRevealed(boolean revealed) {
		this.revealed = revealed;
	}

}
