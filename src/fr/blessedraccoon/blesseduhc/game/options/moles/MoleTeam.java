package fr.blessedraccoon.blesseduhc.game.options.moles;

import java.util.ArrayList;
import java.util.List;

public class MoleTeam {
	
	private List<Mole> moles;
	
	public MoleTeam() {
		this.moles = new ArrayList<>();
	}
	
	public void addMole(Mole m) {
		if (m != null) this.moles.add(m);
	}
	
	public void removeMole(Mole m) {
		this.moles.remove(m);
	}

	public List<Mole> getMoles() {
		return moles;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for(Mole mole : this.moles) {
			sb.append(mole); 
		}
		return sb.toString();
	}

}
