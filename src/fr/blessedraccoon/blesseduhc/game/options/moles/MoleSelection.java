package fr.blessedraccoon.blesseduhc.game.options.moles;

import java.util.Collections;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import fr.blessedraccoon.blessedtimer.timer.StandardTimer;
import fr.blessedraccoon.blessedtimer.timer.TimeListener;
import fr.blessedraccoon.blessedtimer.timer.TimerManager;
import fr.blessedraccoon.blesseduhc.game.GameTeam;
import fr.blessedraccoon.blesseduhc.game.UHCManager;
import net.md_5.bungee.api.ChatColor;

public class MoleSelection implements TimeListener {
	
	private MoleManager moleManager;
	private int triggerTime;
	private int molesPerTeam;
	private int nbMoleTeams;
	
	public MoleSelection(MoleManager moleManager, int trigger, int molesPerTeam, int nbMoleTeams) {
		this.moleManager = moleManager;
		this.triggerTime = trigger;
		this.molesPerTeam = molesPerTeam;
		this.nbMoleTeams = nbMoleTeams;
		TimerManager.getInstance().getTimer(UHCManager.UHC_TIMER_NAME).addTrigger(this);
	}

	@Override
	public int getTriggerTime() {
		return triggerTime;
	}

	@Override
	public void trigger(StandardTimer timer, long time) {
		List<Team> teams = GameTeam.getTeamsWithMinOnlinePlayers(this.molesPerTeam);
		// For all teams that are eligible for mole selection
		for (Team team: teams) {
			List<Player> playersOfTeam = GameTeam.getOnlinePlayersOfTeam(team); // Get the players of team
			// Loop through players of team, select molesPerTeam players total to become moles
			for (int i = 0 ; i < this.molesPerTeam ; i++) {
				// Get a random moleTeam which is not full
				Collections.shuffle(playersOfTeam);
				MoleTeam moleTeam = this.moleManager.getRandomMoleTeam(this.nbMoleTeams);
				final int countTeamsEnoughPlayers = teams.size();
				while (moleTeam.getMoles().size() >= countTeamsEnoughPlayers * this.molesPerTeam / this.nbMoleTeams) {
					moleTeam = this.moleManager.getRandomMoleTeam(this.nbMoleTeams);
				}
				// Add a random mole to the corresponding team of moles
				Mole mole = new Mole(playersOfTeam.remove(0), team);
				moleTeam.addMole(mole);
				
				// Tells the mole they are one
				mole.sendMessage("Vous êtes une taupe !");
				mole.sendMessage("Vous faites maintenant équipe avec d'autres taupes.");
				mole.sendMessage("Trahissez votre ancienne équipe !");
				mole.sendMessage("Discutez avec les autres taupes avec '/t <message>'.");
				mole.sendMessage("Vous pouvez révéler votre nature de taupe aux yeux de tous avec '/reveal'.");
				mole.sendMessage("Cette manoeuvre vous récompensera d'une pomme d'or.");
				mole.sendMessage("Bonne chance !");
			}
		}
		int nb = 1;
		// sends to admin players the mole teams
		for(Player player : GameTeam.getAdminPlayers()) {
			for (MoleTeam team : this.moleManager.getMoleTeams()) {
				player.sendMessage(ChatColor.GOLD + "Team " + nb + " :\n" +team.toString());
				nb++;
			}
		}
	}

}
