package fr.blessedraccoon.blesseduhc.game.options.moles;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.GameTeam;
import fr.blessedraccoon.commandmanager.CommandManager;
import net.md_5.bungee.api.ChatColor;

public class MoleChannel extends CommandManager {
	
	private MoleManager moleManager;
	
	public MoleChannel(MoleManager moleManager) {
		this.moleManager = moleManager;
	}

	@Override
	public int getDeepLevel() {
		return 0;
	}

	@Override
	public String getDescription() {
		return "used to chat with the moles of our team";
	}

	@Override
	public String getName() {
		return "t";
	}

	@Override
	public String getPermission() {
		return null;
	}

	@Override
	public String getSyntax() {
		return "/t <message>";
	}
	
	@Override
	public void fail(Player p) {}
	
	@Override
	public void succeed(Player p) {}

	@Override
	public boolean perform(Player player, String[] args) {
		// for each team of moles
		Mole found = null;
		int index = 1;
		for (MoleTeam team : this.moleManager.getMoleTeams()) {
			found = null;
			// for each mole
			for (Mole mole : team.getMoles()) {
				if (mole.getPlayer().equals(player)) {
					found = mole;
					break;
				}
			}
			// If the player that performs the command in a mole
			if (found != null) {
				// Send message to each mole in this team
				for (Mole mole : team.getMoles()) {
					this.sendMoleMessage(player, index, mole.getPlayer(), String.join(" ", args));
				}
				// Send message to every admin
				for (Player admin : GameTeam.getAdminPlayers()) {
					this.sendMoleMessage(player, index, admin, String.join(" ", args));
				}
				break;
			}
			index++;
		}
		if (found == null) {
			player.sendMessage(ChatColor.RED + "Vous n'êtes pas une taupe.");
			return false;
		}
		return true;
	}
	
	private void sendMoleMessage(Player sender, int teamIndex, Player receiver, String message) {
		receiver.getPlayer().sendMessage(String.format("%s[%d]<%s> %s", ChatColor.GOLD, teamIndex, sender.getName(), message));
	}
	
	

}
