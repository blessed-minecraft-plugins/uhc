package fr.blessedraccoon.blesseduhc.game.options.moles;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.blessedraccoon.commandmanager.CommandManager;
import net.md_5.bungee.api.ChatColor;

public class MoleReveal extends CommandManager {
	
	private MoleManager moleManager;
	
	public MoleReveal(MoleManager moleManager) {
		this.moleManager = moleManager;
	}

	@Override
	public int getDeepLevel() {
		return 0;
	}

	@Override
	public String getDescription() {
		return "reveals a mole";
	}

	@Override
	public String getName() {
		return "reveal";
	}

	@Override
	public String getPermission() {
		return null;
	}

	@Override
	public String getSyntax() {
		return "/reveal";
	}
	
	@Override
	public void succeed(Player p) {}

	@Override
	public boolean perform(Player player, String[] args) {
		Mole mole = this.moleManager.isMole(player);
		if (mole != null) {
			if (! mole.isRevealed()) {
				Bukkit.broadcastMessage(mole.getTeam().getColor() + mole.getPlayer().getName() + ChatColor.GOLD + " se révèle être une taupe !");
				mole.setRevealed(true);
				mole.getPlayer().getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
			} else {
				mole.sendMessage("Vous vous êtes déjà révélé.");
			}
		}
		else {
			player.sendMessage("Vous n'êtes pas une taupe");
		}
		return true;
	}
	
	

}
