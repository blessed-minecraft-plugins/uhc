package fr.blessedraccoon.blesseduhc.game.options.moles;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class MoleManager extends UHCOption {
	
	private List<MoleTeam> moleTeams;
	private MoleSelection selection;
	private MoleChannel channels;
	private MoleReveal reveal;

	public MoleManager(UHC uhc) {
		super(uhc);
		this.moleTeams = new ArrayList<MoleTeam>();
	}
	
	public Mole isMole(Player p) {
		for (MoleTeam moleTeam: this.moleTeams) {
			for(Mole mole: moleTeam.getMoles()) {
				if (mole.getPlayer().equals(p)) {
					return mole;
				}
			}
		}
		return null;
	}

	@Override
	public String getName() {
		return "Moles";
	}
	
	public MoleTeam getRandomMoleTeam(int nbMax) {
		int num = new Random().nextInt(nbMax);
		if (num >= this.moleTeams.size()) {
			for (int i = 0 ; i <= num ; i++) {
				this.moleTeams.add(new MoleTeam());
			}
		}
		return this.moleTeams.get(num);
	}
	
	@Override
	public void prepareOption() throws UHCException {
		this.selection = new MoleSelection(
			this,
			BlessedUHC.CONFIG.getInt("gameplay.options.Moles.moleRevealTime"),
			BlessedUHC.CONFIG.getInt("gameplay.options.Moles.molesPerTeam"),
			BlessedUHC.CONFIG.getInt("gameplay.options.Moles.moleTeams")
		);
		this.channels = new MoleChannel(this);
		this.reveal = new MoleReveal(this);
		BlessedUHC.getUHCPlugin().getCommand(this.channels.getName()).setExecutor(this.channels);
		BlessedUHC.getUHCPlugin().getCommand(this.reveal.getName()).setExecutor(this.reveal);
	}

	@Override
	public void startOption() throws UHCException {
		System.out.println("Démarrage du Taupe Gun");
		
	}

	public List<MoleTeam> getMoleTeams() {
		return moleTeams;
	}

	public MoleSelection getSelection() {
		return selection;
	}

	public MoleChannel getChannels() {
		return channels;
	}

	public MoleReveal getReveal() {
		return reveal;
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.EPIC;
	}

	@Override
	public String getDescription() {
		return "In each team, a mole is selected after 30 minutes. This mole must betray their team and stay with their new mole team.";
	}

}
