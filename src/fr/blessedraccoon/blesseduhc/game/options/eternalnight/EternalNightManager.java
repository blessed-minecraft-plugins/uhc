package fr.blessedraccoon.blesseduhc.game.options.eternalnight;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class EternalNightManager extends UHCOption {

	public EternalNightManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		BlessedUHC.getOverworld().setTime(18000);
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
		BlessedUHC.getOverworld().setTime(18000);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.EPIC;
	}

	@Override
	public String getName() {
		return "EternalNight";
	}
	
	@Override
	public String getDescription() {
		return "Settles Eternal Night during the event.";
	}

}
