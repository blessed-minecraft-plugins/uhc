package fr.blessedraccoon.blesseduhc.game.options;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;

public abstract class UHCOption extends CommandPart implements UHC {
	
	private UHC uhc;

	private int ID = -1;
	
	public UHCOption(UHC uhc) {
		this.uhc = uhc;
		if (this.ID == -1) {
			this.ID = OptionIDGenerator.generate();
		}
	}

	@Override
	public final void prepare() throws UHCException {
		if (this.uhc != null) {
			this.uhc.prepare();
		}
		this.prepareOption();
	}

	@Override
	public final void start() throws UHCException {
		if (this.uhc != null) {
			this.uhc.start();
		}
		this.startOption();
	}
	
	public abstract void prepareOption() throws UHCException;
	public abstract void startOption() throws UHCException;
	
	public final int getID() {
		return ID;
	}
	
	/**
	 * TODO TO TEST...
	 * @param ID
	 */
	public void removeOption(int ID) {
		  if (uhc == null || ! (uhc instanceof UHCOption)) {
		    return;
		  }
		  UHCOption option = (UHCOption) uhc;
		  if (option.getID() == ID) {
			  option.setUHC(null);
			  uhc = option.getNested();
		  } else {
			  option.removeOption(ID);
		  }
	}

	
	public UHC getNested() {
		return this.uhc;
	}
	
	@Override
	public boolean perform(Player player, String[] args) {
		if (Boolean.parseBoolean(args[this.getDeepLevel()])) {
			UHCManager.getInstance().addOption(getID());
			System.out.println("has activated " + this.getName());
		} else {
			UHCManager.getInstance().removeOption(getID());
		}
		return true;
	}
	
	public void manageFromConfig() {
		if (BlessedUHC.CONFIG.getBoolean("gameplay.options." + this.getName() + ".activate")) {
			UHCManager.getInstance().addOption(getID());
		} else {
			UHCManager.getInstance().removeOption(getID());
		}
	}
	
	@Override
	public String getPermission() {
		return "uhc.option.activate";
	}

	@Override
	public String getSyntax() {
		return "/uhc option enable " + this.getName() + "[true | false]";
	}

	
	@Override
	public int getDeepLevel() {
		return 3;
	}
	
	public void setUHC(UHC uhc) {
		this.uhc = uhc;
	}
	
	public List<UHCOption> recap() {
		List<UHCOption> list = new ArrayList<>();
		if (this.uhc != null && this.uhc instanceof UHCOption) {
			list.addAll(((UHCOption) this.uhc).recap());
		}
		list.add(this);
		return list;
	}
	
	@Override
	public String toString() {
		ChatColor color = this.getRarity().getColor();
		return color + this.getName();
	}
	
	public TextComponent toTextComponent() {
		TextComponent text = new TextComponent(this.toString());
		text.setBold(true);
		Text description = new Text( this.getRarity().getColor() + this.getDescription());
		text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, description));
		return text;
	}
	
	public abstract OptionRarity getRarity();

}
