package fr.blessedraccoon.blesseduhc.game.options.halfores;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.drop.blocks.BlockBreakUtils;
import fr.blessedraccoon.blesseduhc.game.drop.blocks.DropFromChance;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class HalfOresManager extends UHCOption implements Listener {

	public HalfOresManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {

	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC plugin = BlessedUHC.getUHCPlugin();
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.LEGENDARY;
	}

	@Override
	public String getDescription() {
		return "Only half of ores drop";
	}

	@Override
	public String getName() {
		return "HalfOres";
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		BlockBreakUtils.modifyDrop(e, BlockBreakUtils.ORES.getValues(), new DropFromChance(0.5));
	}

}
