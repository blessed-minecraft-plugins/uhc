package fr.blessedraccoon.blesseduhc.game.options.achievements;

import org.bukkit.Bukkit;
import org.bukkit.World.Environment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;
import net.md_5.bungee.api.ChatColor;

public class AchievementParanoiaManager extends UHCOption implements Listener {

	public AchievementParanoiaManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC plugin = BlessedUHC.getUHCPlugin();
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.EPIC;
	}

	@Override
	public String getDescription() {
		return "Achievements show up in chat like in vanilla minecraft but at the end of each achievement it shows the coordinates of the player who earned that achievement.";
	}

	@Override
	public String getName() {
		return "AchievementParanoia";
	}
	
	@EventHandler
	public void onAchievement(PlayerAdvancementDoneEvent e) {
		Player p = e.getPlayer();
		String worldName = "";
		if (p.getWorld().getEnvironment() == Environment.NORMAL) worldName = "l'Overworld";
		else if (p.getWorld().getEnvironment() == Environment.NETHER) worldName = "le Nether";
		else if (p.getWorld().getEnvironment() == Environment.THE_END) worldName = "l'End";
		else worldName = "Inconnu";
		if (e.getPlayer().getAdvancementProgress(e.getAdvancement()).getRemainingCriteria().size() == 0) {
			Bukkit.broadcastMessage(String.format("%s est dans %s, aux coordonnées (%.1f; %.1f; %.1f)",
					e.getPlayer().getName(),
					ChatColor.GOLD + worldName + ChatColor.RESET,
					p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ()));
		}
		
	}

}
