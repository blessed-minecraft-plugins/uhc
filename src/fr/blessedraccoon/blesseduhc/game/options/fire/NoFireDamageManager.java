package fr.blessedraccoon.blesseduhc.game.options.fire;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class NoFireDamageManager extends UHCOption {

	public NoFireDamageManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {

	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.FIRE_DAMAGE, false);
		BlessedUHC.getNether().setGameRule(GameRule.FIRE_DAMAGE, false);
		BlessedUHC.getEnd().setGameRule(GameRule.FIRE_DAMAGE, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.RARE;
	}

	@Override
	public String getDescription() {
		return "Everyone is immune to fire and lava";
	}

	@Override
	public String getName() {
		return "NoFireDamage";
	}

}
