package fr.blessedraccoon.blesseduhc.game.options.fire;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class NoFireSpreadManager extends UHCOption {

	public NoFireSpreadManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {

	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.DO_FIRE_TICK, false);
		BlessedUHC.getNether().setGameRule(GameRule.DO_FIRE_TICK, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.COMMON;
	}

	@Override
	public String getDescription() {
		return "The fire no longer spreads around";
	}

	@Override
	public String getName() {
		return "NoFireSpread";
	}

}
