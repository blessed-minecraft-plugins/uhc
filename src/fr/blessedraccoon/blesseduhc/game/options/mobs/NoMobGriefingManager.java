package fr.blessedraccoon.blesseduhc.game.options.mobs;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class NoMobGriefingManager extends UHCOption {

	public NoMobGriefingManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {
		
	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.MOB_GRIEFING, false);
		BlessedUHC.getNether().setGameRule(GameRule.MOB_GRIEFING, false);
		BlessedUHC.getEnd().setGameRule(GameRule.MOB_GRIEFING, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.RARE;
	}

	@Override
	public String getDescription() {
		return "Mobs no longer grief the map: no creeper explosions nor stealing Endermen.";
	}

	@Override
	public String getName() {
		return "NoMobGriefing";
	}

}
