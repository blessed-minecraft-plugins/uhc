package fr.blessedraccoon.blesseduhc.game.options.mobs;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class NoMobLootManager extends UHCOption {

	public NoMobLootManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {

	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.DO_MOB_LOOT, false);
		BlessedUHC.getNether().setGameRule(GameRule.DO_MOB_LOOT, false);
		BlessedUHC.getEnd().setGameRule(GameRule.DO_MOB_LOOT, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.EPIC;
	}

	@Override
	public String getDescription() {
		return "Mobs no longer drop things";
	}

	@Override
	public String getName() {
		return "NoMobLoot";
	}

}
