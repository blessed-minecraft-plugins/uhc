package fr.blessedraccoon.blesseduhc.game.options.mobs;

import org.bukkit.GameRule;

import fr.blessedraccoon.blesseduhc.BlessedUHC;
import fr.blessedraccoon.blesseduhc.game.UHC;
import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.options.OptionRarity;
import fr.blessedraccoon.blesseduhc.game.options.UHCOption;

public class MobFreeManager extends UHCOption {

	public MobFreeManager(UHC uhc) {
		super(uhc);
	}

	@Override
	public void prepareOption() throws UHCException {

	}

	@Override
	public void startOption() throws UHCException {
		BlessedUHC.getOverworld().setGameRule(GameRule.DO_MOB_SPAWNING, false);
		BlessedUHC.getNether().setGameRule(GameRule.DO_MOB_SPAWNING, false);
		BlessedUHC.getEnd().setGameRule(GameRule.DO_MOB_SPAWNING, false);
	}

	@Override
	public OptionRarity getRarity() {
		return OptionRarity.EPIC;
	}

	@Override
	public String getDescription() {
		return "Mobs don't spawn during the event";
	}

	@Override
	public String getName() {
		return "MobFree";
	}

}
