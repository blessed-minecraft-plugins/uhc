package fr.blessedraccoon.blesseduhc.game.drop.blocks;

import java.util.function.Function;

import org.bukkit.inventory.ItemStack;

public class DropMultiplicator implements Function<ItemStack, ItemStack> {
	
	private double rate;
	
	public DropMultiplicator(double rate) {
		this.rate = rate;
	}

	@Override
	public ItemStack apply(ItemStack itemStack) {
		System.out.println("drop x" + rate);
		final int amount = (int) Math.round(itemStack.getAmount() * rate);
		itemStack.setAmount(amount);
		return itemStack;
	}
	
	public double getRate() {
		return rate;
	}
	
	public void setRate(double rate) {
		this.rate = rate;
	}

}
