package fr.blessedraccoon.blesseduhc.game.drop.blocks;

import java.util.function.Function;

import org.bukkit.inventory.ItemStack;

public class DropFromChance implements Function<ItemStack, ItemStack> {
	
	private double rate;
	
	public DropFromChance(double rate) {
		if (rate < 0 || rate > 1) {
			rate = 0.5;
		}
		this.rate = rate;
	}

	@Override
	public ItemStack apply(ItemStack itemStack) {
		if (Math.random() > rate) {
			itemStack.setAmount(0);
		}
		return itemStack;
	}
	
	public double getRate() {
		return rate;
	}
	
	public void setRate(double rate) {
		this.rate = rate;
	}

}
