package fr.blessedraccoon.blesseduhc.game.drop.blocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.destroystokyo.paper.MaterialSetTag;

public class BlockBreakUtils {
	
	public static final MaterialSetTag ORES = new MaterialSetTag(null, Arrays.asList(
			Material.COAL_ORE,
			Material.IRON_ORE,
			Material.GOLD_ORE,
			Material.DIAMOND_ORE,
			Material.EMERALD_ORE,
			Material.LAPIS_ORE,
			Material.REDSTONE_ORE
			/*, Material.NETHER_QUARTZ_ORE Not supported now: "no legacy" */
	));
	
	public static final MaterialSetTag LEAVES = new MaterialSetTag(null, Arrays.asList(
			Material.ACACIA_LEAVES,
			Material.BIRCH_LEAVES,
			Material.DARK_OAK_LEAVES,
			Material.JUNGLE_LEAVES,
			Material.OAK_LEAVES,
			Material.SPRUCE_LEAVES
	));
	
	public static void modifyDrop(BlockBreakEvent e, Collection<Material> materials, Function<ItemStack, ItemStack> multiplicator) {
		if (verify(e, materials)) {
			List<ItemStack> items = new ArrayList<>(e.getBlock().getDrops());
			for (ItemStack itemStack: items) {
				itemStack = multiplicator.apply(itemStack);
				if (itemStack.getAmount() == 0) {
					e.getBlock().getWorld().spawnParticle(Particle.SLIME, e.getBlock().getLocation(), 8);
				} else {
					e.getPlayer().getWorld().dropItemNaturally(e.getBlock().getLocation(), itemStack);
				}
			}
		}
	}
	
	private static boolean verify(BlockBreakEvent e, Collection<Material> materials) {
		Collection<String> names = materials.stream().map(m -> m.toString()).collect(Collectors.toList());
		if (names.contains(e.getBlock().getType().toString())) {
			e.setDropItems(false);
			return true;
		}
		return false;
	}
	
	public static void setDrop(BlockBreakEvent e, Collection<Material> materials, List<ItemStack> stacks) {
		if (verify(e, materials)) {
			if (stacks != null) {
				for (ItemStack stack: stacks) {
					if (stack.getAmount() != 0) {
						e.getPlayer().getWorld().dropItemNaturally(e.getBlock().getLocation(), stack);
					}
				}
			}
		}
	}
	
	public static void setDrop(BlockBreakEvent e, Collection<Material> materials, ItemStack stack) {
		if (stack != null) {
			setDrop(e, materials, Arrays.asList(stack));
		}
	}

}
