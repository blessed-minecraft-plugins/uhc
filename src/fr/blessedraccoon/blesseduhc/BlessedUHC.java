package fr.blessedraccoon.blesseduhc;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import fr.blessedraccoon.blesseduhc.game.DeathControl;
import fr.blessedraccoon.blesseduhc.game.UHCManager;

public class BlessedUHC extends JavaPlugin {

    public static FileConfiguration CONFIG;
    public static final String PLUGIN_NAME = "BlessedUHC";
    
    private static BlessedUHC uhc;

    @Override
    public void onEnable() {
    	this.getConfig().addDefault("overworld", "world");
    	this.getConfig().addDefault("nether", "world_nether");
    	this.getConfig().addDefault("end", "world_end");
    	this.getConfig().addDefault("gameplay.spawn.x", 0);
    	this.getConfig().addDefault("gameplay.spawn.y", 0);
    	this.getConfig().addDefault("gameplay.spawn.z", 0);
    	this.getConfig().addDefault("gameplay.spread.minDistance", 300);
    	this.getConfig().addDefault("gameplay.spread.maxRange", 500);
    	this.getConfig().addDefault("gameplay.spread.respectTeams", true);
    	// ------------------------------------------------------------------------
    	this.getConfig().addDefault("gameplay.borders.beginSize", 1000);
    	this.getConfig().addDefault("gameplay.borders.endSize", 50);
    	this.getConfig().addDefault("gameplay.borders.beginShrinkTime", 3600);
    	this.getConfig().addDefault("gameplay.borders.shrinkDuration", 1800);
    	
    	this.getConfig().addDefault("gameplay.pvpTime", 1200);
    	// ------------------------------------------------------------------------
    	this.getConfig().addDefault("gameplay.options.Moles.activate", false);
    	
    	// -------------------------------- MOLES ---------------------------------
    	this.getConfig().addDefault("gameplay.options.Moles.molesPerTeam", 1);
    	this.getConfig().addDefault("gameplay.options.Moles.moleRevealTime", 1800);
    	this.getConfig().addDefault("gameplay.options.Moles.moleTeams", 1);
    	

		this.getConfig().options().copyDefaults(true);
		saveConfig();

        CONFIG = this.getConfig();
        getCommand("uhc").setExecutor(UHCManager.getInstance());
        getServer().getPluginManager().registerEvents(new DeathControl(), this);
        
        // -----------------------
        uhc = this;
    }

    @Override
    public void onDisable() {
        System.out.println("The UHC plugin shuts down");
    }
    
    public static World getOverworld() {
    	return Bukkit.getServer().getWorld(CONFIG.getString("overworld"));
    }
    
    public static World getNether() {
    	return Bukkit.getServer().getWorld(CONFIG.getString("nether"));
    }
    
    public static World getEnd() {
    	return Bukkit.getServer().getWorld(CONFIG.getString("end"));
    }
    
    public static BlessedUHC getUHCPlugin() {
    	return uhc;
    }
    
}
