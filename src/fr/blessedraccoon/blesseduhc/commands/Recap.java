package fr.blessedraccoon.blesseduhc.commands;

import fr.blessedraccoon.commandmanager.CommandManager;
import fr.blessedraccoon.commandmanager.RecapCommandValue;

public class Recap extends RecapCommandValue {

    public Recap(CommandManager manager) {
        super(manager);
    }

	@Override
	public String getPermission() {
		return "uhc.recap";
	}
    
}
