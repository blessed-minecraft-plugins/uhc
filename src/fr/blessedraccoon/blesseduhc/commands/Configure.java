package fr.blessedraccoon.blesseduhc.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.commands.configure.*;
import fr.blessedraccoon.commandmanager.CommandPart;

public class Configure extends CommandPart {

    public Configure() {
        this.addSubcommand(new SetSpawnX());
        this.addSubcommand(new SetSpawnZ());
        this.addSubcommand(new SetSpreadMaxRange());
        this.addSubcommand(new SetSpreadMinDistance());
        this.addSubcommand(new SetRespectTeams());
        this.addSubcommand(new SetBeginSize());
        this.addSubcommand(new SetEndSize());
        this.addSubcommand(new SetPvPTime());
        this.addSubcommand(new SetBeginShrinkTime());
        this.addSubcommand(new SetShrinkDuration());
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "allows to configure UHC parameters";
    }

    @Override
    public String getName() {
        return "configure";
    }

    @Override
    public String getSyntax() {
        return "/uhc configure <option>";
    }

    @Override
    public boolean perform(Player arg0, String[] arg1) {
        return false;
    }

	@Override
	public String getPermission() {
		return "uhc.configure";
	}
    
}
