package fr.blessedraccoon.blesseduhc.commands;

import fr.blessedraccoon.commandmanager.CommandManager;
import fr.blessedraccoon.commandmanager.HelpCommandPattern;

public class UHCHelp extends HelpCommandPattern {

    public UHCHelp(CommandManager manager) {
        super(manager);
    }

	@Override
	public String getPermission() {
		return "uhc.help";
	}
    
}
