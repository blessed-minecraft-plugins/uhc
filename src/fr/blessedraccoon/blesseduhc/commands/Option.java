package fr.blessedraccoon.blesseduhc.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.commands.options.Draw;
import fr.blessedraccoon.blesseduhc.commands.options.Enable;
import fr.blessedraccoon.blesseduhc.commands.options.OptionList;
import fr.blessedraccoon.commandmanager.CommandPart;

public class Option extends CommandPart {
	
	public Option() {
		this.addSubcommand(new Enable());
		this.addSubcommand(new Draw());
		this.addSubcommand(new OptionList());
	}

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Root command for configuring options";
    }

    @Override
    public String getName() {
        return "option";
    }

    @Override
    public String getSyntax() {
        return "/uhc option";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        return false;
    }

	@Override
	public String getPermission() {
		return "uhc.option";
	}
    
}
