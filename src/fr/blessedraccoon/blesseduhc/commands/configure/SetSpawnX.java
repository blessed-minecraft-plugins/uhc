package fr.blessedraccoon.blesseduhc.commands.configure;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;
import fr.blessedraccoon.commandmanager.CommandValuable;

public class SetSpawnX extends CommandPart implements CommandValuable {

    @Override
    public int getDeepLevel() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "Sets the center X coordinate, to spread the players apart from.";
    }

    @Override
    public String getName() {
        return "setSpawnX";
    }

    @Override
    public String getSyntax() {
        return "/uhc configure setSpawnX <Integer>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
            int x = Integer.parseInt(args[this.getDeepLevel()]);
            UHCManager.getInstance().getCenter().setX(x);
            return true;
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The parameter you gave is not an Integer.");
            return false;
        }
    }

    @Override
    public String getValue() {
        return "" + UHCManager.getInstance().getCenter().getX();
    }

	@Override
	public String getPermission() {
		return "uhc.configure.setSpawn";
	}
    
}
