package fr.blessedraccoon.blesseduhc.commands.configure;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;
import fr.blessedraccoon.commandmanager.CommandValuable;

public class SetBeginShrinkTime extends CommandPart implements CommandValuable {

    @Override
    public String getValue() {
        return UHCManager.getInstance().getWorldborder().getShrinkBorders().getTriggerTime() + " (in seconds)";
    }

    @Override
    public int getDeepLevel() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "sets the time of the border where it starts to shrink";
    }

    @Override
    public String getName() {
        return "setBeginShrinkTime";
    }

    @Override
    public String getSyntax() {
        return "/uhc configure setBeginShrinkTime <Seconds>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
            int time = Integer.parseInt(args[this.getDeepLevel()]) / 10;
            UHCManager.getInstance().getWorldborder().getShrinkBorders().setTrigger(time);
            return true;
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The parameter you gave is not an Integer.");
            return false;
        }
    }

	@Override
	public String getPermission() {
		return "uhc.configure.setBeginShrinkTime";
	}
    
}
