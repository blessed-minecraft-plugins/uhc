package fr.blessedraccoon.blesseduhc.commands.configure;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;
import fr.blessedraccoon.commandmanager.CommandValuable;

public class SetShrinkDuration extends CommandPart implements CommandValuable {

    @Override
    public String getValue() {
        return "" + UHCManager.getInstance().getWorldborder().getShrinkDuration() + " (in seconds)";
    }

    @Override
    public int getDeepLevel() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "sets the shrink duration of the border";
    }

    @Override
    public String getName() {
        return "setShrinkDuration";
    }

    @Override
    public String getSyntax() {
        return "/uhc configure setShrinkDuration <Seconds>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
            int size = Integer.parseInt(args[this.getDeepLevel()]) / 10;
            UHCManager.getInstance().getWorldborder().setShrinkDuration(size);
            return true;
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The parameter you gave is not an Integer.");
            return false;
        }
    }

	@Override
	public String getPermission() {
		return "uhc.configure.setShrinkDuration";
	}
    
}
