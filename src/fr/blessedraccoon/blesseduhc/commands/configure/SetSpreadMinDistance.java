package fr.blessedraccoon.blesseduhc.commands.configure;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;
import fr.blessedraccoon.commandmanager.CommandValuable;

public class SetSpreadMinDistance extends CommandPart implements CommandValuable{

    @Override
    public int getDeepLevel() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "Sets the minimum distance between two players (or teams) after spreading.";
    }

    @Override
    public String getName() {
        return "setSpreadMinDistance";
    }

    @Override
    public String getSyntax() {
        return "/uhc configure setSpreadMinDistance <Integer>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
            int x = Integer.parseInt(args[this.getDeepLevel()]);
            UHCManager.getInstance().getSpreadPlayers().setMinDistance(x);
            return true;
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The parameter you gave is not an Integer.");
            return false;
        }
    }

    @Override
    public String getValue() {
        return UHCManager.getInstance().getSpreadPlayers().getMinDistance() + " (radius)";
    }

	@Override
	public String getPermission() {
		return "uhc.configure.setSpreadMinDistance";
	}
    
}
