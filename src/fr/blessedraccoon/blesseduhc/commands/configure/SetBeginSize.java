package fr.blessedraccoon.blesseduhc.commands.configure;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;
import fr.blessedraccoon.commandmanager.CommandValuable;

public class SetBeginSize extends CommandPart implements CommandValuable {

    @Override
    public String getValue() {
        return UHCManager.getInstance().getWorldborder().getBeginSize() + " (Diameter)";
    }

    @Override
    public int getDeepLevel() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "sets the begin size of the border";
    }

    @Override
    public String getName() {
        return "setBeginSize";
    }

    @Override
    public String getSyntax() {
        return "/uhc configure setBeginSize <Diameter>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
            int size = Integer.parseInt(args[this.getDeepLevel()]);
            UHCManager.getInstance().getWorldborder().setBeginSize(size);
            return true;
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The parameter you gave is not an Integer.");
            return false;
        }
    }

	@Override
	public String getPermission() {
		return "uhc.configure.setBeginSize";
	}
    
}
