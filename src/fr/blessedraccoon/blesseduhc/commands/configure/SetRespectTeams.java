package fr.blessedraccoon.blesseduhc.commands.configure;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;
import fr.blessedraccoon.commandmanager.CommandValuable;

public class SetRespectTeams extends CommandPart implements CommandValuable {

    @Override
    public int getDeepLevel() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "Sets whether the teams must be took into account, or not.";
    }

    @Override
    public String getName() {
        return "setRespectTeams";
    }

    @Override
    public String getSyntax() {
        return "/uhc configure setRespectTeams <Boolean>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        if ((!args[this.getDeepLevel()].equalsIgnoreCase("true")) &&
            (!args[this.getDeepLevel()].equalsIgnoreCase("false"))) {
                return false;
        }
        boolean value = Boolean.parseBoolean(args[this.getDeepLevel()]);
        UHCManager.getInstance().getSpreadPlayers().setRespectTeams(value);
        return true;
    }

    @Override
    public String getValue() {
        return "" + UHCManager.getInstance().getSpreadPlayers().isRespectTeams();
    }

	@Override
	public String getPermission() {
		return "uhc.configure.setRespectTeams";
	}
    
}
