package fr.blessedraccoon.blesseduhc.commands.configure;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;
import fr.blessedraccoon.commandmanager.CommandValuable;

public class SetPvPTime extends CommandPart implements CommandValuable {

    @Override
    public int getDeepLevel() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "Sets the time where PvP is allowed.";
    }

    @Override
    public String getName() {
        return "setPvPTime";
    }

    @Override
    public String getSyntax() {
        return "/uhc configure setPvPTime <Boolean>";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
            int value = Integer.parseInt(args[this.getDeepLevel()]) / 10;
            UHCManager.getInstance().getAllowPvp().setTriggerTime(value);
        return true;
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The parameter you gave is not an Integer.");
            return false;
        }  
    }

    @Override
    public String getValue() {
        return "" + UHCManager.getInstance().getAllowPvp().getTriggerTime();
    }

	@Override
	public String getPermission() {
		return "uhc.setPVPTime";
	}
    
}