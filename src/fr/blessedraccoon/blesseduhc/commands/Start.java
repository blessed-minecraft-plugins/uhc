package fr.blessedraccoon.blesseduhc.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCException;
import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;
import fr.blessedraccoon.commandmanager.CommandValuable;

public class Start extends CommandPart implements CommandValuable {

    @Override
    public String getValue() {
        return "" + UHCManager.getInstance().isStarted();
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Starts the UHC";
    }

    @Override
    public String getName() {
        return "start";
    }

    @Override
    public String getSyntax() {
        return "/uhc start";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
			UHCManager.getInstance().getFullGame().start();
			return true;
		} catch (UHCException e) {
			e.printStackTrace();
			return false;
		}
    }

	@Override
	public String getPermission() {
		return "uhc.start";
	}
    
}
