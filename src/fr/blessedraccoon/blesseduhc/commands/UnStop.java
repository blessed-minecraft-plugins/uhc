package fr.blessedraccoon.blesseduhc.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;

public class UnStop extends CommandPart {

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Unstops the UHC, if it was paused";
    }

    @Override
    public String getName() {
        return "unStop";
    }

    @Override
    public String getSyntax() {
        return "/uhc unStop";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        UHCManager.getInstance().setStarted(true);
        UHCManager.getInstance().getTimer().setRunning(true);
        return true;
    }

	@Override
	public String getPermission() {
		return "uhc.unStop";
	}
    
}
