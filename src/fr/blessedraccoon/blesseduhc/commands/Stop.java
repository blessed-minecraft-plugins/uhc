package fr.blessedraccoon.blesseduhc.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;

public class Stop extends CommandPart {

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Stops the UHC";
    }

    @Override
    public String getName() {
        return "stop";
    }

    @Override
    public String getSyntax() {
        return "/uhc stop";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        UHCManager.getInstance().setStarted(false);
        UHCManager.getInstance().getTimer().setRunning(false);
        return true;
    }

	@Override
	public String getPermission() {
		// TODO Auto-generated method stub
		return null;
	}
    
}
