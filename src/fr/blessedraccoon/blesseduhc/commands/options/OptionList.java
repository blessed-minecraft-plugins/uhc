package fr.blessedraccoon.blesseduhc.commands.options;

import org.bukkit.entity.Player;

import fr.blessedraccoon.commandmanager.CommandPart;

public class OptionList extends CommandPart {
	
	public OptionList() {
		this.addSubcommand(new OptionListAll());
		this.addSubcommand(new OptionListEnabled());
	}

	@Override
	public int getDeepLevel() {
		return 2;
	}

	@Override
	public String getDescription() {
		return "lists all the options";
	}

	@Override
	public String getName() {
		return "list";
	}

	@Override
	public String getPermission() {
		return "uhc.option.list";
	}

	@Override
	public String getSyntax() {
		return "/uhc option list";
	}

	@Override
	public boolean perform(Player arg0, String[] arg1) {
		return false;
	}

}
