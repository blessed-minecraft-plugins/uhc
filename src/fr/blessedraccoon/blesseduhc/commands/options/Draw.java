package fr.blessedraccoon.blesseduhc.commands.options;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;

public class Draw extends CommandPart {

	@Override
	public int getDeepLevel() {
		return 2;
	}

	@Override
	public String getDescription() {
		return "draws randomly the options of the UHC, according to their respective rarity";
	}

	@Override
	public String getName() {
		return "draw";
	}

	@Override
	public String getPermission() {
		return "uhc.option.draw";
	}

	@Override
	public String getSyntax() {
		return "/uhc option draw";
	}

	@Override
	public boolean perform(Player player, String[] args) {
		UHCManager.getInstance().drawOptions();
		return true;
	}

}
