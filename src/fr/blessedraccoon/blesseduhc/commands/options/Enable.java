package fr.blessedraccoon.blesseduhc.commands.options;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.options.UHCOptions;
import fr.blessedraccoon.commandmanager.CommandPart;

public class Enable extends CommandPart {
	
	public Enable() {
		for(UHCOptions option: UHCOptions.values()) {
			this.addSubcommand(option.getOption());
		}
	}

    @Override
    public int getDeepLevel() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "Root command for enabling options";
    }

    @Override
    public String getName() {
        return "enable";
    }

    @Override
    public String getSyntax() {
        return "/uhc option enable";
    }

    @Override
    public boolean perform(Player player, String[] args) {
        return false;
    }

	@Override
	public String getPermission() {
		return "uhc.option.enable";
	}
    
}
