package fr.blessedraccoon.blesseduhc.commands.options;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;

public class OptionListAll extends CommandPart {

	@Override
	public int getDeepLevel() {
		return 3;
	}

	@Override
	public String getDescription() {
		return "returns all the option, implemented in the plugin";
	}

	@Override
	public String getName() {
		return "all";
	}

	@Override
	public String getPermission() {
		return "uhc.option.list.all";
	}

	@Override
	public String getSyntax() {
		return "/uhc option list all";
	}
	
	@Override
	public void succeed(Player p) {}

	@Override
	public boolean perform(Player player, String[] args) {
		UHCManager.getInstance().recapAllOptions(player);
		return true;
	}

}
