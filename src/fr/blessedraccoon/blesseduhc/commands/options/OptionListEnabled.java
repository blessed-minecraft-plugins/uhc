package fr.blessedraccoon.blesseduhc.commands.options;

import org.bukkit.entity.Player;

import fr.blessedraccoon.blesseduhc.game.UHCManager;
import fr.blessedraccoon.commandmanager.CommandPart;

public class OptionListEnabled extends CommandPart {

	@Override
	public int getDeepLevel() {
		return 3;
	}

	@Override
	public String getDescription() {
		return "tells which option is enabled";
	}

	@Override
	public String getName() {
		return "enabled";
	}

	@Override
	public String getPermission() {
		return "uhc.option.list.enabled";
	}

	@Override
	public String getSyntax() {
		return "/uhc option list enabled";
	}
	
	@Override
	public void succeed(Player p) {}

	@Override
	public boolean perform(Player player, String[] args) {
		UHCManager.getInstance().recapCurrentOptions(player);
		return true;
	}

}
